use std::{
    mem::transmute,
    hash::{
        Hash,
        Hasher
    },
    fmt::{
        Debug,
        Display
    }, ops::{Index, IndexMut}
};

use base64::{
    alphabet::URL_SAFE,
    engine::fast_portable::{
        FastPortable,
        FastPortableConfig
    }
};



const RAW_ADDRESS_SIZE:usize=33;
const USER_FRIENDLY_ADDRESS_SIZE:usize=36;



/// The flag changes the way smart-contract reacts to the message.
#[derive(Debug,Clone,Copy,PartialEq,Eq)]
#[repr(u8)]
pub enum UserFriendlyFlag{
    Bounceable=0x11,
    NonBounceable=0x51,

    /// If the address should not be accepted by software running in the production network.
    TestNetOnly=0x80,

    /// For any custom flag.
    Else
}

impl UserFriendlyFlag{
    /// Builds a flag if it's the supported one,
    /// otherwise returns `UserFriendlyFlag::Else`.
    pub fn new(flag:u8)->UserFriendlyFlag{
        unsafe{
            match transmute(flag){
                UserFriendlyFlag::TestNetOnly=>UserFriendlyFlag::TestNetOnly,
                UserFriendlyFlag::Bounceable=>UserFriendlyFlag::Bounceable,
                UserFriendlyFlag::NonBounceable=>UserFriendlyFlag::NonBounceable,
                _=>UserFriendlyFlag::Else
            }
        }
    }

    /// Builds a custom flag.
    pub unsafe fn custom(flag:u8)->UserFriendlyFlag{
        transmute(flag)
    }
}


#[test]
fn test_flag1(){
    let raw_address="0:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";

    let raw=RawAddress::from_raw_str(raw_address).unwrap();

    let custom_flag=unsafe{UserFriendlyFlag::custom(0x01)};
    let user_friendly=raw.to_user_friendly(custom_flag);

    assert_eq!(UserFriendlyFlag::Else,user_friendly.flag());
}

#[test]
fn test_flag2(){
    let bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";

    let user_friendly=UserFriendlyAddress::from_user_friendly_str(bounceable).unwrap();

    assert_eq!(user_friendly.flag(),UserFriendlyFlag::Bounceable);
}

#[test]
fn test_flag3(){
    let non_bounceable="UQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNqQF";
    let user_friendly=UserFriendlyAddress::from_user_friendly_str(non_bounceable).unwrap();

    assert_eq!(user_friendly.flag(),UserFriendlyFlag::NonBounceable);
}


/// Represents TON workchain.
#[derive(Debug,Clone,Copy,PartialEq,Eq)]
#[repr(u8)]
pub enum Workchain{
    BaseChain=0x00,
    /// For any custom workchain.
    Else,
    MasterChain=0xff
}


#[test]
fn test_workchain1(){
    let raw_address="0:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";

    let raw=RawAddress::from_raw_str(raw_address).unwrap();

    assert_eq!(raw.workchain(),Workchain::BaseChain);
}

#[test]
fn test_workchain2(){
    let raw_address="1:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";

    let raw=RawAddress::from_raw_str(raw_address).unwrap();

    assert_eq!(raw.workchain(),Workchain::Else);
}

#[test]
fn test_workchain3(){
    let bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";

    let user_friendly=UserFriendlyAddress::from_user_friendly_str(bounceable).unwrap();

    assert_eq!(user_friendly.workchain(),Workchain::BaseChain);
}


/// Possible parse errors.
#[derive(Debug,Clone,Copy,PartialEq,Eq)]
pub enum AddressParseError{
    /// Returned if there is no separating ':' in a raw address.
    InvalidFormat,

    /// Returned if the length of the decoded address doesn't equal to 33 signs (for raw addresses)
    /// or 36 signs (for user-friendly addresses).
    InvalidLength,

    /// Returned if the workchain id of an address is not a decimal number.
    InvalidWorkchainId,

    /// Returns if the account id of a raw address conatains non-hexadecimal digits.
    InvalidHexDigit,

    /// Returns if the given address is not base64urlsafe-encoded.
    BadBase64String,

    /// Returns if the given verification code doesn't match the generated one.
    InvalidVerificationCode
}



/// A raw smart contract address.
/// 
/// The address size is 33 bytes.
/// The first byte is for the workchain id.
/// The other 32 bytes are for the account id.
#[derive(Eq,Ord)]
pub struct RawAddress{
    ptr:*mut u8,
    capacity:usize
}

impl RawAddress{
    /// Creates an instance with uninitialized data.
    pub unsafe fn new()->RawAddress{
        let buffer=Vec::with_capacity(RAW_ADDRESS_SIZE);
        Self::from_bytes(buffer)
    }

    /// Creates an instance from raw bytes.
    /// 
    /// The `bytes` length must be 33 bytes.
    #[inline(always)]
    pub unsafe fn from_bytes(bytes:Vec<u8>)->RawAddress{
        let capacity=bytes.capacity();
        let ptr=bytes.leak().get_unchecked_mut(0);

        Self{
            ptr,
            capacity
        }
    }

    /// Returns raw bytes.
    #[inline(always)]
    pub const fn as_bytes(&self)->&[u8]{
        unsafe{
            std::slice::from_raw_parts(self.ptr,RAW_ADDRESS_SIZE)
        }
    }

    /// Returns the workchain id.
    #[inline(always)]
    pub fn workchain_id(&self)->i8{
        unsafe{
            let ptr=self.ptr as *mut i8;
            ptr.read()
        }
    }

    /// Returns the workchain id.
    #[inline(always)]
    pub fn workchain_id_u8(&self)->u8{
        unsafe{
            self.ptr.read()
        }
    }

    /// Returns the workchain.
    pub fn workchain(&self)->Workchain{
        unsafe{
            let ptr=self.ptr as *mut Workchain;
            match ptr.read(){
                Workchain::BaseChain=>Workchain::BaseChain,
                Workchain::MasterChain=>Workchain::MasterChain,
                _=>Workchain::Else
            }
        }
    }

    /// Returns the 32-byte account id (256 bits address inside the workchain).
    #[inline(always)]
    pub const fn account_id(&self)->&[u8]{
        unsafe{
            let ptr=self.ptr.offset(1);
            std::slice::from_raw_parts(ptr,RAW_ADDRESS_SIZE-1)
        }
    }

    /// Returns the 32-byte account id (256 bits address inside the workchain).
    #[inline(always)]
    pub fn account_id_mut(&mut self)->&mut [u8]{
        unsafe{
            let ptr=self.ptr.offset(1);
            std::slice::from_raw_parts_mut(ptr,RAW_ADDRESS_SIZE-1)
        }
    }

    /// Returns the `index` byte of the address.
    pub fn get(&self,index:usize)->Option<&u8>{
        unsafe{
            if index<RAW_ADDRESS_SIZE{
                let ptr=self.ptr.offset(index as isize);
                Some(&*ptr)
            }
            else{
                None
            }
        }
    }

    /// Returns the `index` byte of the address.
    pub fn get_mut(&mut self,index:usize)->Option<&mut u8>{
        unsafe{
            if index<RAW_ADDRESS_SIZE{
                let ptr=self.ptr.offset(index as isize);
                Some(&mut *ptr)
            }
            else{
                None
            }
        }
    }

    /// Returns the `index` byte of the address.
    #[inline(always)]
    pub unsafe fn get_unchecked(&self,index:usize)->&u8{
        unsafe{
            let ptr=self.ptr.offset(index as isize);
            &*ptr
        }
    }

    /// Returns the `index` byte of the address.
    #[inline(always)]
    pub unsafe fn get_unchecked_mut(&mut self,index:usize)->&mut u8{
        unsafe{
            let ptr=self.ptr.offset(index as isize);
            &mut *ptr
        }
    }

    /// Parses an address in raw (hex) format.
    /// 
    /// Returns `AddressParseError::InvalidFormat` if threse no seperating ":".
    /// 
    /// Returns `AddressParseError::InvalidWorkchainId` if the first part (workchain id) is not a decimal number.
    /// 
    /// Returns `AddressParseError::InvalidHexDigit` if there is a non-hexadecimal number in the second part.
    /// 
    /// Returns `AddressParseError::InvalidLength`, if the length of the decoded address is not 33.
    pub fn from_raw_str(address:&str)->Result<RawAddress,AddressParseError>{
        let mut bytes=Vec::with_capacity(RAW_ADDRESS_SIZE);

        let Some((workchain_id,mut account_id))=address.split_once(":") else {
            return Err(AddressParseError::InvalidFormat)
        };

        let Ok(workchain_id)=workchain_id.parse::<i8>() else {
            return Err(AddressParseError::InvalidWorkchainId)
        };

        bytes.push(unsafe{transmute(workchain_id)});
        while !account_id.is_empty(){
            if let Ok(byte)=u8::from_str_radix(&account_id[0..2],16){
                bytes.push(byte);
                account_id=&account_id[2..];
            }
            else{
                return Err(AddressParseError::InvalidHexDigit)
            }
        }

        if bytes.len()!=RAW_ADDRESS_SIZE{
            return Err(AddressParseError::InvalidLength)
        }

        Ok(unsafe{RawAddress::from_bytes(bytes)})
    }

    /// Parses an address in user-friendly (base64-encoded) format.
    /// 
    /// Ignores all the flags (`UserFriendlyFlag`) and the verification code.
    /// 
    /// Returns `AddressParseError::BadBase64String`, if `address` is not base64urlsafe-encoded.
    /// 
    /// Returns `AddressParseError::InvalidLength`, if the length of the decoded address is not 36.
    /// 
    /// Returns `AddressParseError::InvalidVerificationCode`, if the code is wrong.
    pub fn from_user_friendly_str(address:&str)->Result<RawAddress,AddressParseError>{
        let config=FastPortable::from(&URL_SAFE,FastPortableConfig::new());
        let Ok(mut address)=base64::decode_engine(address,&config) else {
            return Err(AddressParseError::BadBase64String)
        };

        if address.len()!=USER_FRIENDLY_ADDRESS_SIZE{
            return Err(AddressParseError::InvalidLength)
        }

        let verification=crc16::State::<crc16::XMODEM>::calculate(&address[0..34]).to_be_bytes();

        if verification[0]!=address[34] || verification[1]!=address[35]{
            return Err(AddressParseError::InvalidVerificationCode)
        }

        address.pop();
        address.pop();
        address.remove(0);

        Ok(unsafe{RawAddress::from_bytes(address)})
    }

    /// Parses any address format.
    /// 
    /// Returns `AddressParseError::InvalidFormat` if any error accures.
    pub fn from_any_str(address:&str)->Result<RawAddress,AddressParseError>{
        if let Ok(address)=Self::from_raw_str(address){
            return Ok(address)
        }

        if let Ok(address)=Self::from_user_friendly_str(address){
            return Ok(address)
        }

        Err(AddressParseError::InvalidFormat)
    }

    /// Builds a raw address from a user-friendly one.
    pub fn from_user_friendly(address:&UserFriendlyAddress)->RawAddress{
        let mut bytes=Vec::from(address.as_bytes());

        bytes.pop();
        bytes.pop();
        bytes.remove(0);

        unsafe{RawAddress::from_bytes(bytes)}
    }

    /// Sets the workchain.
    pub fn set_workchain(&mut self,workchain:Workchain){
        unsafe{
            self.ptr.write(workchain as u8)
        }
    }

    /// Builds a user-friendly address and converts it to a string.
    pub fn to_user_friendly_str(&self,address_flag:UserFriendlyFlag)->String{
        self.to_user_friendly(address_flag).to_string()
    }

    /// Builds a user-friendly address.
    pub fn to_user_friendly(&self,address_flag:UserFriendlyFlag)->UserFriendlyAddress{
        let mut address=Vec::from(self.as_bytes());
        address.reserve_exact(3);

        address.insert(0,address_flag as u8);

        let verification=crc16::State::<crc16::XMODEM>::calculate(&address).to_be_bytes();

        address.push(verification[0]);
        address.push(verification[1]);

        unsafe{UserFriendlyAddress::from_bytes(address)}
    }

    /// Returns a raw smart contract address in the standart format.
    /// 
    /// A raw smart contract address consists of (`workchain_id`, `account_id`) in this format:
    /// `[decimal workchain_id]:[64 hexadecimal digits with account_id]`.
    fn to_string(&self)->String{
        let mut raw_address=String::with_capacity(66);

        let bytes=self.as_bytes();

        raw_address.push_str(&format!("{}:",bytes[0]));

        for &byte in &bytes[1..]{
            raw_address.push_str(&format!("{byte:02x}"))
        }

        raw_address
    }
}

impl Drop for RawAddress{
    fn drop(&mut self){
        unsafe{
            Vec::from_raw_parts(
                self.ptr,
                RAW_ADDRESS_SIZE,
                self.capacity
            );
        }
    }
}

impl PartialEq for RawAddress{
    fn eq(&self,other:&Self)->bool{
        let bytes1=self.as_bytes();
        let bytes2=other.as_bytes();

        bytes1==bytes2
    }
}

impl PartialOrd for RawAddress{
    fn partial_cmp(&self,other:&Self)->Option<std::cmp::Ordering>{
        let bytes1=self.as_bytes();
        let bytes2=other.as_bytes();
        bytes1.partial_cmp(bytes2)
    }
}

impl Hash for RawAddress{
    fn hash<H:Hasher>(&self,state:&mut H){
        self.as_bytes().hash(state)
    }
}

impl Clone for RawAddress{
    fn clone(&self)->RawAddress{
        unsafe{
            RawAddress::from_bytes(Vec::from(self.as_bytes()))
        }
    }
}

impl Debug for RawAddress{
    fn fmt(&self,f:&mut std::fmt::Formatter<'_>)->std::fmt::Result{
        self.as_bytes().fmt(f)
    }
}

impl Display for RawAddress{
    fn fmt(&self,f:&mut std::fmt::Formatter<'_>)->std::fmt::Result{
        f.write_str(&self.to_string())
    }
}

impl From<UserFriendlyAddress> for RawAddress{
    fn from(value:UserFriendlyAddress)->RawAddress{
        value.to_raw()
    }
}

impl Index<usize> for RawAddress{
    type Output=u8;

    fn index(&self,index:usize)->&Self::Output{
        self.get(index).expect("Out of bounds")
    }
}

impl IndexMut<usize> for RawAddress{
    fn index_mut(&mut self,index:usize)->&mut Self::Output{
        self.get_mut(index).expect("Out of bounds")
    }
}

unsafe impl Sync for RawAddress{}
unsafe impl Send for RawAddress{}



#[test]
fn test_raw1(){
    let raw_address="0:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";
    let bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";
    let _non_bounceable="UQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNqQF";

    let from_hex=RawAddress::from_raw_str(raw_address).unwrap();
    let from_bounceable=RawAddress::from_user_friendly_str(bounceable).unwrap();

    assert_eq!(from_hex,from_bounceable);
}

#[test]
fn test_raw2(){
    let raw_address="0:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";
    let _bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";
    let _non_bounceable="UQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNqQF";

    let from_hex=RawAddress::from_raw_str(raw_address).unwrap();

    assert_eq!(raw_address,from_hex.to_string());
}

#[test]
fn test_raw3(){
    let raw_address="0:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";
    let bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";
    let _non_bounceable="UQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNqQF";

    let from_any1=RawAddress::from_any_str(raw_address).unwrap();
    let from_any2=RawAddress::from_any_str(bounceable).unwrap();

    assert_eq!(from_any1,from_any2);
}

#[test]
fn test_raw4(){
    let raw_address="0:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";
    let _bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";
    let non_bounceable="UQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNqQF";

    let from_hex=RawAddress::from_raw_str(raw_address).unwrap();

    assert_eq!(from_hex.to_user_friendly_str(UserFriendlyFlag::NonBounceable),non_bounceable);
}

#[test]
fn test_raw5(){
    let raw_address="0:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";
    let _bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";
    let non_bounceable="UQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNqQF";

    let raw=RawAddress::from_raw_str(raw_address).unwrap();
    let raw_user_friendly=raw.to_user_friendly(UserFriendlyFlag::NonBounceable);
    let user_friendly=UserFriendlyAddress::from_user_friendly_str(non_bounceable).unwrap();

    assert_eq!(raw_user_friendly,user_friendly);
}

#[test]
fn test_raw6(){
    let raw_address="0:ba60bfbd527c0cd2d70c63630c50a498af015b987adaad1d4a9e287f604536";

    let error=RawAddress::from_raw_str(raw_address);

    assert_eq!(error,Err(AddressParseError::InvalidLength));
}

#[test]
fn test_raw7(){
    let raw_address="0:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";

    let raw=RawAddress::from_raw_str(raw_address).unwrap();

    assert_eq!(Workchain::BaseChain as u8,raw[0]);
}

/// A user-friendly smart contract address.
/// 
/// The address size is 36 bytes.
/// 
/// The first byte is for the flag (it changes the way a smart-contract reacts to the message).
/// The second byte is for the workchain id.
/// The next 32 bytes are for the account id.
/// And the last two ones are for the verification code (CRC16-XMODEM).
#[derive(Eq,Ord)]
pub struct UserFriendlyAddress{
    ptr:*mut u8,
    capacity:usize
}

impl UserFriendlyAddress{
    /// Creates an instance with uninitialized data.
    pub unsafe fn new()->UserFriendlyAddress{
        let buffer=Vec::with_capacity(USER_FRIENDLY_ADDRESS_SIZE);
        Self::from_bytes(buffer)
    }

    /// Creates an instance from raw bytes.
    /// 
    /// The `bytes` length must be 36 bytes.
    #[inline(always)]
    pub unsafe fn from_bytes(bytes:Vec<u8>)->UserFriendlyAddress{
        let capacity=bytes.capacity();
        let ptr=bytes.leak().get_unchecked_mut(0);

        Self{
            ptr,
            capacity
        }
    }

    /// Returns raw bytes.
    #[inline(always)]
    pub const fn as_bytes(&self)->&[u8]{
        unsafe{
            std::slice::from_raw_parts(self.ptr,USER_FRIENDLY_ADDRESS_SIZE)
        }
    }

    /// Returns the flag of the address.
    pub fn flag(&self)->UserFriendlyFlag{
        unsafe{
            let ptr=self.ptr as *mut UserFriendlyFlag;
            match ptr.read(){
                UserFriendlyFlag::TestNetOnly=>UserFriendlyFlag::TestNetOnly,
                UserFriendlyFlag::Bounceable=>UserFriendlyFlag::Bounceable,
                UserFriendlyFlag::NonBounceable=>UserFriendlyFlag::NonBounceable,
                _=>UserFriendlyFlag::Else
            }
        }
    }

    /// Returns the workchain id.
    #[inline(always)]
    pub fn workchain_id(&self)->i8{
        unsafe{
            let ptr=self.ptr.offset(1) as *mut i8;
            ptr.read()
        }
    }

    /// Returns the workchain id.
    #[inline(always)]
    pub fn workchain_id_u8(&self)->u8{
        unsafe{
            self.ptr.offset(1).read()
        }
    }

    /// Returns the workchain.
    pub fn workchain(&self)->Workchain{
        unsafe{
            let ptr=self.ptr.offset(1) as *mut Workchain;
            match ptr.read(){
                Workchain::BaseChain=>Workchain::BaseChain,
                Workchain::MasterChain=>Workchain::MasterChain,
                _=>Workchain::Else
            }
        }
    }

    /// Returns the 32-byte account id (256 bits address inside the workchain).
    #[inline(always)]
    pub fn account_id(&self)->&[u8]{
        unsafe{
            let ptr=self.ptr.offset(2);
            std::slice::from_raw_parts(ptr,USER_FRIENDLY_ADDRESS_SIZE-2)
        }
    }

    /// Returns the 32-byte account id (256 bits address inside the workchain).
    #[inline(always)]
    pub fn account_id_mut(&mut self)->&mut [u8]{
        unsafe{
            let ptr=self.ptr.offset(2);
            std::slice::from_raw_parts_mut(ptr,USER_FRIENDLY_ADDRESS_SIZE-2)
        }
    }

    /// Returns the verification code.
    #[inline(always)]
    pub fn verification_code(&self)->u16{
        unsafe{
            let byte1=self.ptr.offset(34);
            let byte2=self.ptr.offset(35);
            u16::from_be_bytes([byte1.read(),byte2.read()])
        }
    }

    /// Returns the `index` byte of the address.
    pub fn get(&self,index:usize)->Option<&u8>{
        unsafe{
            if index<USER_FRIENDLY_ADDRESS_SIZE{
                let ptr=self.ptr.offset(index as isize);
                Some(&*ptr)
            }
            else{
                None
            }
        }
    }

    /// Returns the `index` byte of the address.
    pub fn get_mut(&mut self,index:usize)->Option<&mut u8>{
        unsafe{
            if index<USER_FRIENDLY_ADDRESS_SIZE{
                let ptr=self.ptr.offset(index as isize);
                Some(&mut *ptr)
            }
            else{
                None
            }
        }
    }

    /// Returns the `index` byte of the address.
    #[inline(always)]
    pub unsafe fn get_unchecked(&self,index:usize)->&u8{
        unsafe{
            let ptr=self.ptr.offset(index as isize);
            &*ptr
        }
    }

    /// Returns the `index` byte of the address.
    #[inline(always)]
    pub unsafe fn get_unchecked_mut(&mut self,index:usize)->&mut u8{
        unsafe{
            let ptr=self.ptr.offset(index as isize);
            &mut *ptr
        }
    }

    /// Parses an address in raw (hex) format.
    /// 
    /// Returns `AddressParseError::InvalidFormat` if threse no seperating ":".
    /// 
    /// Returns `AddressParseError::InvalidWorkchainId` if the first part (workchain id) is not a decimal number.
    /// 
    /// Returns `AddressParseError::InvalidHexDigit` if there is a non-hexadecimal number in the second part.
    /// 
    /// Returns `AddressParseError::InvalidLength`, if the length of the decoded address is not 33.
    pub fn from_raw_str(address:&str,address_flag:UserFriendlyFlag)->Result<UserFriendlyAddress,AddressParseError>{
        let mut bytes=Vec::with_capacity(USER_FRIENDLY_ADDRESS_SIZE);

        bytes.push(address_flag as u8);

        let Some((workchain_id,mut account_id))=address.split_once(":") else {
            return Err(AddressParseError::InvalidFormat)
        };

        let Ok(workchain_id)=workchain_id.parse::<i8>() else {
            return Err(AddressParseError::InvalidWorkchainId)
        };

        bytes.push(unsafe{transmute(workchain_id)});
        while !account_id.is_empty(){
            if let Ok(byte)=u8::from_str_radix(&account_id[0..2],16){
                bytes.push(byte);
                account_id=&account_id[2..];
            }
            else{
                return Err(AddressParseError::InvalidHexDigit)
            }
        }

        if bytes.len()!=RAW_ADDRESS_SIZE+1{
            return Err(AddressParseError::InvalidLength)
        }

        let verification=crc16::State::<crc16::XMODEM>::calculate(&bytes).to_be_bytes();

        bytes.push(verification[0]);
        bytes.push(verification[1]);

        Ok(unsafe{UserFriendlyAddress::from_bytes(bytes)})
    }

    /// Parses an address in user-friendly (base64-encoded) format.
    /// 
    /// Checks the verification code.
    /// 
    /// Returns `AddressParseError::BadBase64String`, if `address` is not base64urlsafe-encoded.
    /// 
    /// Returns `AddressParseError::InvalidLength`, if the length of the decoded address is not 36.
    /// 
    /// Returns `AddressParseError::InvalidVerificationCode`, if the code is wrong.
    pub fn from_user_friendly_str(address:&str)->Result<UserFriendlyAddress,AddressParseError>{
        let config=FastPortable::from(&URL_SAFE,FastPortableConfig::new());
        let Ok(address)=base64::decode_engine(address,&config) else {
            return Err(AddressParseError::BadBase64String)
        };

        if address.len()!=USER_FRIENDLY_ADDRESS_SIZE{
            return Err(AddressParseError::InvalidLength)
        }

        let verification=crc16::State::<crc16::XMODEM>::calculate(&address[0..34]).to_be_bytes();

        if verification[0]!=address[34] || verification[1]!=address[35]{
            return Err(AddressParseError::InvalidVerificationCode)
        }

        Ok(unsafe{UserFriendlyAddress::from_bytes(address)})
    }

    /// Builds a user-friendly address from a raw one.
    #[inline(always)]
    pub fn from_raw(address:&RawAddress,address_flag:UserFriendlyFlag)->UserFriendlyAddress{
        address.to_user_friendly(address_flag)
    }

    /// Sets the flag.
    /// 
    /// Run `UserFriendlyAddress::recalculate` after finishing customization.
    #[inline(always)]
    pub fn set_flag(&mut self,flag:UserFriendlyFlag){
        unsafe{
            self.ptr.write(flag as u8)
        }
    }

    /// Sets the workchain.
    /// 
    /// Run `UserFriendlyAddress::recalculate` after finishing customization.
    #[inline(always)]
    pub fn set_workchain(&mut self,workchain:Workchain){
        unsafe{
            self.ptr.offset(1).write(workchain as u8)
        }
    }

    /// Updates the verification code.
    pub fn recalculate(&mut self){
        let bytes=&self.as_bytes()[0..34];
        let verification=crc16::State::<crc16::XMODEM>::calculate(bytes).to_be_bytes();
        unsafe{
            *self.get_unchecked_mut(35)=verification[0];
            *self.get_unchecked_mut(36)=verification[1];
        }
    }

    /// Builds a raw address.
    #[inline(always)]
    pub fn to_raw(&self)->RawAddress{
        RawAddress::from_user_friendly(self)
    }

    /// Returns a user-friendly address in the standart format (base64urlsafe-encoded).
    #[inline(always)]
    pub fn to_string(&self)->String{
        let config=FastPortable::from(&URL_SAFE,FastPortableConfig::new());
        base64::encode_engine(self.as_bytes(),&config)
    }
}

impl Drop for UserFriendlyAddress{
    fn drop(&mut self){
        unsafe{
            Vec::from_raw_parts(
                self.ptr,
                USER_FRIENDLY_ADDRESS_SIZE,
                self.capacity
            );
        }
    }
}

impl PartialEq for UserFriendlyAddress{
    fn eq(&self,other:&Self)->bool{
        let bytes1=self.as_bytes();
        let bytes2=other.as_bytes();

        bytes1==bytes2
    }
}

impl PartialOrd for UserFriendlyAddress{
    fn partial_cmp(&self,other:&Self)->Option<std::cmp::Ordering>{
        let bytes1=self.as_bytes();
        let bytes2=other.as_bytes();
        bytes1.partial_cmp(bytes2)
    }
}

impl Hash for UserFriendlyAddress{
    fn hash<H:Hasher>(&self,state:&mut H){
        self.as_bytes().hash(state)
    }
}

impl Clone for UserFriendlyAddress{
    fn clone(&self)->UserFriendlyAddress{
        unsafe{
            UserFriendlyAddress::from_bytes(Vec::from(self.as_bytes()))
        }
    }
}

impl Debug for UserFriendlyAddress{
    fn fmt(&self,f:&mut std::fmt::Formatter<'_>)->std::fmt::Result{
        self.as_bytes().fmt(f)
    }
}

impl Display for UserFriendlyAddress{
    fn fmt(&self,f:&mut std::fmt::Formatter<'_>)->std::fmt::Result{
        f.write_str(&self.to_string())
    }
}

impl Index<usize> for UserFriendlyAddress{
    type Output=u8;

    fn index(&self,index:usize)->&Self::Output{
        self.get(index).expect("Out of bounds")
    }
}

impl IndexMut<usize> for UserFriendlyAddress{
    fn index_mut(&mut self,index:usize)->&mut Self::Output{
        self.get_mut(index).expect("Out of bounds")
    }
}

unsafe impl Sync for UserFriendlyAddress{}
unsafe impl Send for UserFriendlyAddress{}



#[test]
fn test_user1(){
    let raw_address="0:ba60bfbd527c0cd2d70c6396630c50a498af015b987adaad1d4a9e287f604536";
    let _bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";
    let non_bounceable="UQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNqQF";

    let user_friendly=UserFriendlyAddress::from_raw_str(raw_address,UserFriendlyFlag::NonBounceable).unwrap();

    assert_eq!(user_friendly.to_string(),non_bounceable);
}

#[test]
fn test_user2(){
    let bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";

    let user_friendly=UserFriendlyAddress::from_user_friendly_str(bounceable).unwrap();

    assert_eq!(user_friendly.to_string(),bounceable);
}

#[test]
fn test_user3(){
    let bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnb";

    let error=UserFriendlyAddress::from_user_friendly_str(bounceable);

    assert_eq!(error,Err(AddressParseError::InvalidVerificationCode));
}

#[test]
fn test_user4(){
    let bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";

    let user_friendly=UserFriendlyAddress::from_user_friendly_str(bounceable).unwrap();

    assert_eq!(UserFriendlyFlag::Bounceable as u8,user_friendly[0]);
    assert_eq!(Workchain::BaseChain as u8,user_friendly[1]);
}

#[test]
fn test_user5(){
    let bounceable="EQC6YL-9UnwM0tcMY5ZjDFCkmK8BW5h62q0dSp4of2BFNvnA";

    let user_friendly=UserFriendlyAddress::from_user_friendly_str(bounceable).unwrap();

    assert_eq!(UserFriendlyFlag::Bounceable as u8,user_friendly[40]);
}